#!/usr/bin/env python
# -*- coding: utf-8 -*-

from operator import mul
from fractions import Fraction
from math import factorial, exp

import matplotlib.pyplot as plt

# C(n,k) = n! / k!(n-k)!
C = lambda n, k: reduce(mul, (Fraction(n - i, i + 1) for i in xrange(k)), 1)


def P1(k, n, N, D_f):
	return C(n=D_f, k=k) * C(n=N-D_f, k=n-k) / C(n=N, k=n)


def P2(k, n, N, D_f):
	ro = Fraction(D_f, N)
	return C(n=n, k=k) * ro ** k * (1 - ro) ** (n - k)


def P3(k, n, N, D_f):
	l = n * Fraction(D_f, N)
	return exp(-l) * l ** k / factorial(k)


# H0 => (D_f = D_provider)
# H1 => (D_f = D_customer)

# P = P1 or P2 or P3
# betta = summ(0 <= l <= c) [P(x=l | H1)]
def find_betta(n, N, D_customer, c, P):
	betta = 0.0

	for i in xrange(0, c + 1):
		if n - i > N - D_customer:
			continue
		betta += P(i, n, N, D_customer)
	return betta


# c = max(k) -> summ(k+1 <= l <= n) [P(x=l | H0)] <= alpha
def find_c(values, alpha):
	c, summ = 0, 0.0
	for i in xrange(len(values) - 1, -1, -1):
		summ += values[i]
		if summ > alpha:
			c = i
			break
	return c


def plot_and_save(x, y_list, title='figure', legend=[], axes_names=('x','y')):
	fig, axes = plt.subplots()
	plt.title(title)

	line_types = ['-', '-', '--']
	for i, y in enumerate(y_list):
		axes.plot(n_values, y, line_types[i], lw = 3)

	axes.grid(alpha = 0.5, linestyle = 'dashed', linewidth = 0.5)
	axes.legend(legend, loc = 'best')
	axes.set_xlabel(axes_names[0])
	axes.set_ylabel(axes_names[1])

	fig.savefig(title + '.png')


# input values
N = 200
D_provider = 10
D_customer = 40
n_min = 40
n_max = 80
step = 4
alpha = 0.1

n_values = range(n_min, n_max + 1, step)
c1, c2, c3 = [], [], []
betta1, betta2, betta3 = [], [], []

for n in n_values:
	print 'n: ', n
	P1_values, P2_values, P3_values = [], [], []

	for k in xrange(0, D_provider + 1):
		P1_values.append(P1(k, n, N, D_provider))
		P2_values.append(P2(k, n, N, D_provider))
		P3_values.append(P3(k, n, N, D_provider))

	c1.append(find_c(P1_values, alpha))
	c2.append(find_c(P2_values, alpha))
	c3.append(find_c(P3_values, alpha))

	betta1.append(find_betta(n, N, D_customer, c1[-1], P1))
	betta2.append(find_betta(n, N, D_customer, c2[-1], P2))
	betta3.append(find_betta(n, N, D_customer, c3[-1], P3))


plot_and_save(
	n_values, 
	[betta1, betta2, betta3], 
	title='betta_from_n', 
	legend=['betta1: P1', 'betta2: P2', 'betta3: P3'],
	axes_names=['n', 'betta'])

plot_and_save(
	n_values, 
	[c1, c2, c3], 
	title='c_from_n', 
	legend=['c1: P1', 'c2: P2', 'c3: P3'],
	axes_names=['n', 'c'])