-- 1. Функции
-- 1.1 add_city(city_name text, country_name text) returns integer – добавить новый го-
-- род в БД, возвратив его id. Если город или страна с такими названиями уже есть,
-- то использовать существующие записи БД.

CREATE OR REPLACE FUNCTION add_city(city_name TEXT, country_name TEXT) 
RETURNS INT 
AS $$
DECLARE
	id_city INT;
	id_country INT;
BEGIN
	SELECT city_id FROM city JOIN country USING(country_id) 
	WHERE city=city_name 
	AND country=country_name INTO id_city;
	IF NOT FOUND THEN
		SELECT country_id FROM country WHERE country=country_name INTO id_country;
		IF NOT FOUND THEN
			INSERT INTO country(country) VALUES(country_name) 
			    RETURNING country_id INTO id_country;
			INSERT INTO city(city, country_id) VALUES(city_name, id_country)
			    RETURNING city_id INTO id_city;
		ELSE
			INSERT INTO city(city, country_id) VALUES(city_name, id_country)
			    RETURNING city_id INTO id_city;
		END IF;
	END IF;

	RETURN id_city;
END
$$ LANGUAGE plpgsql;

-- 1.2 find_email(email text) returns setof text – возвратить имена людей с заданным
-- email’ом из таблиц customer и staff.

CREATE OR REPLACE FUNCTION find_email(person_email TEXT) 
RETURNS SETOF TEXT 
AS $$
DECLARE
	f_name TEXT;
	l_name TEXT;
	tables TEXT[] := array['customer', 'staff'];
	sql TEXT;
BEGIN
	FOR t_ind IN 1..array_upper(tables, 1) LOOP
		sql := 'SELECT first_name, last_name FROM '||tables[t_ind]||' WHERE email='||
		quote_literal(person_email);
	  	FOR f_name, l_name IN EXECUTE sql LOOP
			RETURN NEXT f_name||' '||l_name; 
		END LOOP;
	END LOOP;
END
$$ LANGUAGE plpgsql;

-- 2 Триггеры
-- Добавить к таблице film поля:
-- • inventory_count – число связанных записей в таблице inventory;
-- • rental_count – число связанных записей в таблице rental.

ALTER TABLE film ADD COLUMN inventory_count INT DEFAULT 0;
ALTER TABLE film ADD COLUMN rental_count INT DEFAULT 0;

-- установка стартовых значений
UPDATE film
SET inventory_count = t.count 
FROM (
    SELECT film_id, count(film_id) FROM inventory GROUP BY(film_id)
)t
WHERE film.film_id = t.film_id;

UPDATE film
SET rental_count = t.count 
FROM (
	SELECT film_id, count(film_id) 
	FROM rental JOIN inventory using(inventory_id) 
	GROUP BY film_id
)t
WHERE film.film_id = t.film_id;

-- Создать триггеры, автоматически обновляющие поля inventory_count и rental_count
-- при изменениях в таблицах inventory и rental соответственно. Проверить корректность
-- работы триггеров при выполнении INSERT, UPDATE и DELETE запросов к таблицам
-- inventory и rental.

CREATE OR REPLACE FUNCTION on_inventory() 
RETURNS TRIGGER 
AS $$
DECLARE
    new_film_id INT;
    old_film_id INT;
    i_count INT;
BEGIN
    IF    TG_OP = 'INSERT' THEN
        new_film_id = NEW.film_id;
        UPDATE film SET inventory_count = inventory_count + 1 
        WHERE film_id = new_film_id;
        RETURN NEW;
    ELSIF TG_OP = 'UPDATE' THEN
        old_film_id = OLD.film_id;
        new_film_id = NEW.film_id;
        UPDATE film SET inventory_count = inventory_count - 1 
        WHERE film_id = old_film_id;
        UPDATE film SET inventory_count = inventory_count + 1 
        WHERE film_id = new_film_id;

        SELECT count(*) FROM rental WHERE inventory_id = OLD.inventory_id
        INTO i_count;
        UPDATE film SET rental_count = rental_count - i_count 
        WHERE film_id = old_film_id;
        UPDATE film SET rental_count = rental_count + i_count
        WHERE film_id = new_film_id;

        RETURN NEW;
    ELSIF TG_OP = 'DELETE' THEN
        old_film_id = OLD.film_id;
        UPDATE film SET inventory_count = inventory_count - 1 
        WHERE film_id = old_film_id;
        RETURN OLD;
    END IF;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER t_inventory
AFTER INSERT OR UPDATE OR DELETE ON inventory 
FOR EACH ROW EXECUTE PROCEDURE on_inventory();

-- проверка работы тригера
SELECT film_id, inventory_count FROM film WHERE film_id=33;
--  film_id | inventory_count 
-- ---------+-----------------
--       33 |               0
INSERT INTO inventory(film_id, store_id) VALUES(33, 1);
INSERT INTO inventory(film_id, store_id) VALUES(33, 2);
--  film_id | inventory_count 
-- ---------+-----------------
--       33 |               2
SELECT film_id, inventory_count FROM film WHERE film_id = 1;
--  film_id | inventory_count 
-- ---------+-----------------
--        1 |               8
UPDATE inventory SET film_id=1 WHERE film_id=33 AND store_id=2;
SELECT film_id, inventory_count FROM film WHERE film_id = 1 OR film_id=33;
--  film_id | inventory_count 
-- ---------+-----------------
--       33 |               1
--        1 |               9
DELETE FROM inventory WHERE film_id=33;
--  film_id | inventory_count 
-- ---------+-----------------
--       33 |               0
SELECT film_id, inventory_count, rental_count FROM film WHERE film_id=7 OR film_id=17;
--  film_id | inventory_count | rental_count 
-- ---------+-----------------+--------------
--       17 |               6 |           18
--        7 |               5 |           15
-- (2 строки)

UPDATE inventory SET film_id=17 WHERE film_id=7;
-- UPDATE 5
SELECT film_id, inventory_count, rental_count FROM film WHERE film_id=7 OR film_id=17;
--  film_id | inventory_count | rental_count 
-- ---------+-----------------+--------------
--       17 |              11 |           33
--        7 |               0 |            0
-- (2 строки)


CREATE OR REPLACE FUNCTION on_rental() 
RETURNS TRIGGER 
AS $$
DECLARE
	new_inventory_id INT;
	old_inventory_id INT;
    new_film_id INT;
    old_film_id INT;
BEGIN
    IF    TG_OP = 'INSERT' THEN
        new_inventory_id = NEW.inventory_id;
        SELECT film_id FROM inventory 
        WHERE inventory_id = new_inventory_id INTO new_film_id;

        UPDATE film SET rental_count = rental_count + 1 
        WHERE film_id = new_film_id;
        RETURN NEW;
    ELSIF TG_OP = 'UPDATE' THEN
        old_inventory_id = OLD.inventory_id;
        new_inventory_id = NEW.inventory_id;
        SELECT film_id FROM inventory 
        WHERE inventory_id = old_inventory_id INTO old_film_id;
        SELECT film_id FROM inventory 
        WHERE inventory_id = new_inventory_id INTO new_film_id;

        UPDATE film SET rental_count = rental_count - 1 
        WHERE film_id = old_film_id;
        UPDATE film SET rental_count = rental_count + 1 
        WHERE film_id = new_film_id;
        RETURN NEW;
    ELSIF TG_OP = 'DELETE' THEN
        old_inventory_id = OLD.inventory_id;
        SELECT film_id FROM inventory 
        WHERE inventory_id = old_inventory_id INTO old_film_id;

        UPDATE film SET rental_count = rental_count - 1 
        WHERE film_id = old_film_id;
        RETURN OLD;
    END IF;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER t_rental
AFTER INSERT OR UPDATE OR DELETE ON rental 
FOR EACH ROW EXECUTE PROCEDURE on_rental();

-- проверка
SELECT film_id, inventory_id, rental_count 
FROM film JOIN inventory USING(film_id) WHERE film_id=10;
--  film_id | inventory_id | rental_count 
-- ---------+--------------+--------------
--       10 |           46 |           23
--       10 |           47 |           23
--       10 |           48 |           23
--       10 |           49 |           23
--       10 |           50 |           23
--       10 |           51 |           23
--       10 |           52 |           23
SELECT count(*) FROM rental WHERE inventory_id > 45 AND inventory_id < 53;
--  count 
-- -------
--     23
INSERT INTO rental(rental_date, inventory_id, customer_id, staff_id) values(NOW(), 46, 10, 1);
SELECT film_id,rental_count FROM film WHERE film_id=10;
--  film_id | rental_count 
-- ---------+--------------
--       10 |           24
SELECT film_id, inventory_id, rental_count
FROM film JOIN inventory USING(film_id) WHERE film_id=11 OR film_id=12;
-- film_id | inventory_id | rental_count 
-- ---------+--------------+--------------
--       11 |           53 |           24
--       11 |           54 |           24
--       11 |           55 |           24
--       11 |           56 |           24
--       11 |           57 |           24
--       11 |           58 |           24
--       11 |           59 |           24
--       12 |           60 |           26
--       12 |           61 |           26
--       12 |           62 |           26
--       12 |           63 |           26
--       12 |           64 |           26
--       12 |           65 |           26
--       12 |           66 |           26
SELECT count(*) FROM rental WHERE inventory_id=53;
--  count 
-- -------
--      5
SELECT count(*) FROM rental WHERE inventory_id=60;
--  count 
-- -------
--      2
UPDATE rental SET inventory_id = 60 WHERE inventory_id = 53;
SELECT count(*) FROM rental WHERE inventory_id=53;
--  count 
-- -------
--      0
SELECT count(*) FROM rental WHERE inventory_id=60;
--  count 
-- -------
--      7
SELECT film_id, rental_count FROM film WHERE film_id=11 OR film_id=12;
--  film_id | rental_count 
-- ---------+--------------
--       11 |           19		== 24 - 5
--       12 |           31		== 26 + 5

-- DELETE 
SELECT max(rental_id) FROM rental;
--   max  
-- -------
--  16052
DELETE FROM rental WHERE rental_id = 16052;
--  film_id | rental_count 
-- ---------+--------------
--       10 |           23